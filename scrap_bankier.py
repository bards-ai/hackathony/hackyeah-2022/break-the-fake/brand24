from bs4 import BeautifulSoup
from urllib.parse import urlparse
import requests
import json
from tqdm import tqdm
import datetime


def get_all_news_links(url: str, pages: int):
    all_links = []
    for i in range(1, pages + 1):
        html = requests.get(f"{url}{i}").text
        soup = BeautifulSoup(html, "html.parser")
        main = soup.find("section", {"id": "articleList"})

        all_news = main.find_all("a")
        links = [news.get("href", default="") for news in all_news]
        links = [
            link for link in links if link.startswith("/wiadomosc/") and len(link) > 13
        ]
        links = [f"https://www.bankier.pl{link}" for link in links]
        all_links += links
    return all_links


def get_article(url_article: str):
    html = requests.get(url_article).text
    soup = BeautifulSoup(html, "html.parser")

    title_html = soup.find("h1")
    title = ""
    if title_html:
        title = title_html.text

    summary_html = soup.find("span", {"class": "lead"})
    summary = ""
    if summary_html:
        summary = summary_html.text.lstrip().rstrip() or " "
    summary

    source_name = urlparse(url_article).netloc.split(".")[1] or " "

    date_html = soup.find("header", {"class": "o-article-header"})
    date = ""
    if date_html:
        time = date_html.find("span", {"class": "a-span"})
        if time:
            date = time.text.strip()

    author_html = soup.find("section", {"class": "o-article-source"})
    author = ""
    if author_html:
        link = author_html.find("a")
        if link:
            author = link.text

    tags_elements = soup.find("section", {"class": "o-tags-cloud-box"})
    if tags_elements:
        tags_elements = tags_elements.find_all("a")
    tags = []
    if tags_elements:
        tags = [tag.text for tag in tags_elements]

    article_html = soup.find("section", {"class": "o-article-content"})
    if article_html:
        article_ps = article_html.find_all("p")[1:]
    text = ""
    if article_ps:
        text = " ".join([p.text for p in article_ps])
        text = " ".join(text.split())

    article = {
        "title": title,
        "summary": summary,
        "link": url_article,
        "source": source_name,
        "date": date,
        "author": author,
        "tags": tags,
        "text": text,
        "category": "",
        "sentiment": "",
        "is_fake": "",
    }

    return article


def get_all_articles(links):
    articles = []
    for link in tqdm(links):
        article = get_article(link)
        articles.append(article)
    return articles


def save_json(object, filename: str):
    with open(filename, "a+", encoding="utf-8") as f:
        json.dump(object, f, ensure_ascii=False, indent=4)


def get_json(filename):
    with open(filename) as f:
        object = json.load(f)
    return object


def run_bankier():
    current_time = datetime.datetime.now()

    links = get_all_news_links("https://www.bankier.pl/wiadomosc/", pages=30)
    links = list(set(links))
    save_json(links, f"bankier-links-30-{current_time}.json")
    links = get_json(f"bankier-links-30-{current_time}.json")
    articles = get_all_articles(links)
    save_json(articles, f"bankier_articles-30-{current_time}.json")


if __name__ == "__main__":
    run_bankier()
