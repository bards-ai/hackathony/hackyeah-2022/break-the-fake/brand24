import os
import deepl
from tqdm import tqdm
from scrap_businessinsider import get_json, save_json

API_KEY = os.environ.get('deeply')
translator = deepl.Translator(API_KEY)


def translate_summaries(articles):
    for article_idx, article in enumerate(tqdm(articles)):
        summary = article["summary"]

        if summary:
            article_translated = translator.translate_text(summary, source_lang="pl", target_lang="EN-US")
            articles[article_idx]["translated_summary"] = article_translated.text
        else:
            articles[article_idx]["translated_summary"] = ""

    return articles


if __name__ == "__main__":
    articles = get_json("businessinsider_articles-30.json")
    articles_translated = translate_summaries(articles)
    save_json(articles_translated, "businessinsider_articles-30-translated.json")
