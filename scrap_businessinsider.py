from bs4 import BeautifulSoup
from urllib.parse import urlparse
import requests
import json
from tqdm import tqdm
import datetime


def get_main_news(main):
    news_divs = main.findAll("div", {"class": "stream-list"})
    main_news = []
    for div in news_divs:
        main_news += div.find_all("a")
    return main_news


def get_all_news_links(url: str, pages: int = 10):
    html = requests.get(url).text
    soup = BeautifulSoup(html, "html.parser")
    main = soup.find("main")

    top_news = soup.find("section", {"class": "single-section-horizontal"}).find_all(
        "a"
    )
    main_news = get_main_news(main)
    popular_news = (
        main.find_next_sibling("div")
        .find("section", {"class": "text-list"})
        .find_all("a")
    )

    all_news = top_news + main_news + popular_news

    for page in range(2, pages + 1):
        html = requests.get(f"{url}?page={page}").text
        soup = BeautifulSoup(html, "html.parser")
        main = soup.find("main")
        all_news += get_main_news(main)

    links = [news.get("href", default="") for news in all_news]

    return links


def get_article(url_article: str):
    html = requests.get(url_article).text
    soup = BeautifulSoup(html, "html.parser")

    title_html = soup.find("h1")
    title = ""
    if title_html:
        title = title_html.text

    summary_html = soup.find("div", {"class": "article_lead"})
    summary = ""
    if summary_html:
        summary = summary_html.text.lstrip().rstrip() or " "

    link = url_article
    source_name = urlparse(url_article).netloc.split(".")[0] or " "

    date_html = soup.find("div", {"class": "article-article_date"})
    date = ""
    if date_html:
        time = date_html.find("time")
        if time:
            date = time.text.strip()

    author_html = soup.find("div", {"class": "article-author_text"})
    author = ""
    if author_html:
        author = author_html.text.strip() or " "

    tags_elements = soup.find("div", {"class": "article-tags"})
    if tags_elements:
        tags_elements = tags_elements.find_all("a")
    tags = []
    if tags_elements:
        tags = [tag.text for tag in tags_elements]

    article_ps = soup.find("section", {"class": "main whitelistPremium"})
    if article_ps:
        article_ps = article_ps.find_all("p", {"class": "article_p"})
    text = ""
    if article_ps:
        text = " ".join([p.text for p in article_ps])
        text = " ".join(text.split())

    article = {
        "title": title,
        "summary": summary,
        "link": link,
        "source": source_name,
        "date": date,
        "author": author,
        "tags": tags,
        "text": text,
        "category": "",
        "sentiment": "",
        "is_fake": "",
    }

    return article


def get_all_articles(links):
    articles = []
    for link in tqdm(links):
        article = get_article(link)
        articles.append(article)
    return articles


def save_json(object, filename: str):
    with open(filename, "a+", encoding="utf-8") as f:
        json.dump(object, f, ensure_ascii=False, indent=4)


def get_json(filename):
    with open(filename) as f:
        object = json.load(f)
    return object


def run_buisnessinsider():
    current_time = datetime.datetime.now()

    links = get_all_news_links("https://businessinsider.com.pl/gospodarka", pages=1)
    save_json(links, f"businessinsider_links-30-{current_time}.json")

    links = get_json(f"businessinsider_links-30-{current_time}.json")
    articles = get_all_articles(links)
    save_json(articles, f"businessinsider_articles-30-{current_time}.json")


if __name__ == "__main__":
    run_buisnessinsider()
