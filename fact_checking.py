import json

import pandas as pd
from sentence_transformers import SentenceTransformer
from PIL import Image
from sklearn.metrics.pairwise import cosine_similarity
from tqdm import tqdm



class FakeNewsDatabase:
    def __init__(self, src_file: str, embedder_path:str):
        self._embedder = SentenceTransformer(embedder_path)

        with open(src_file) as f:
            data = json.load(f)
        texts = [entry['fake_news_title'] for entry in data]

        self._db = {'texts': texts,
                    'embeddings': self.embedd(texts)}

    def search(self, phrase: Image):
        phrase_embedding = self.embedd(phrase)
        sim_vector = cosine_similarity([phrase_embedding], self._db['embeddings'])[0]
        return self._db['texts'], sim_vector

    def embedd(self, texts):
        embeddings = self._embedder.encode(texts)
        return embeddings

if __name__=='__main__':
    fn_db = FakeNewsDatabase('demagog_fake_titles.json','sentence-transformers/distiluse-base-multilingual-cased-v2')
    
    with open('bankier_articles-30-hatespeech.json', 'r') as f_handle:
        data =  json.load(f_handle)

    for entry in data:
        texts, similarities = fn_db.search(entry['text'])
        
        df_fake_sim = pd.DataFrame({"texts":texts, 'similarities':similarities.tolist()})
        df_fake_sim['position'] = df_fake_sim.index
        df_fake_sim.sort_values('similarities', ascending=False, inplace=True)
        df_fake_sim.reset_index(inplace=True)
        
        most_similar = df_fake_sim.iloc[0]

        entry['is_fake'] = most_similar['similarities']
        entry['is_fake_text'] = most_similar['texts']

    with open('bankier_articles-30-fake_news.json', "a+", encoding="utf-8") as f:
        json.dump(data, f, ensure_ascii=False, indent=4)