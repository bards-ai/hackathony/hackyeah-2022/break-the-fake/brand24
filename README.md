# FERA (Finacial Efective Resources )

Nasz projekt zapewnia w pełni automatyczne scrapowanie stron internowych z wiadomościami finansowymi. 
Aktualnie opieramy się na 3 źródłach danych, ale w przyszłości można dodać ich więcej. Są to:
- https://businessinsider.com.pl/
- https://www.money.pl/gospodarka/wiadomosci/
- https://www.bankier.pl/wiadomosc/

Dla każdej strony został napisany osobny scraper, który najpierw ściąga linki do artykułów, na nich się znajdujących, zapisuje je do plików, a następnie ściąga już całe artykuły, które również zapisywane są do osobnych plików. 

W następnym etapie do ściągniętych oraz wstępnie przetworznych danych, dodawane są dodatkowe informacje, takie jak: występowanie mowy nienawiści, występowanie fałszywych informacji, analiza sentymentu, klasyfikacja kategorii artykułu. Tak przygotowane dane są następnie wyświetlane w interfejsie użytkownika.

## Struktura projektu

1. Pliki `scrap_*.py` - odpowiedzialne są za scrapowanie wybranych 3 stron internetowych z informacjami dotyczącymi gospodarki
2. Plik `beat.py` - służy do cyklicznego scrapowania stron internetowych, uruchamiając go należy podać parametr liczby sekund, co którą ściągane są pliki
3. Pliki `add_*.py` oraz `hatespeech.ipynb` i `fact_checking.ipynb` - służą do analizy ściągniętych danych i dodania do nich dodatkowych informacji:
- `hatespeech.ipynb` - o prawdopodobieństwie wystąpienia w artykule mowy nienawiści
- `fact_checking.ipynb` - o prawdopodbieństwie zawierania przez artykuł fałszywych informacji
- `add_category.py` - kategorii, której dotyczy artykuły
- `add_sentiment.py` - o sentymencie danego artykułu
- `add_translation.py` - o tłumaczeniu danego artykułu na język angielski, w celu wykorzystania na nim modeli sztucznej inteligencji o wyższej jakości
- `add_everything.py` - dodaniu informacji o kategorii, sentymencie i tłumaczeniu w jednym pipelinie
4. Plik `scrap_demagog.py` - służy do zescrapowania źródła wiedzy wykorzystywanego w detekcji fake-newsów

## Zebrane dane
Zebrane i przetworzone dane znajdują się w plikach typu `*.json`. Pliki te występują w 2 typach: artykułów oraz linków. Każde scraper najpierw pozyskuje linki do artykułów na danej stronie, a następnie pobiera te artykuły w całości. Nazwa każdego pliku zawiera informację o pochodzeniu (strony www) artykułu lub linku do artykułu. Nazwa pliku wskazuje również na dodatkowy atrybut, o którym została dodana informacja.

## Uruchomienie skryptów
0. `poetry install`
1. `poetry shell`
2. `python beat.py` - uruchomienie cyklicznego, automatycznego zbierania danych
3. `set deeply=[API_KEY]` - do uruchomienia tłumaczenia za pomocą serwisu `deeply` należy założyć darmowe konto i wygenerować klucz, który zapisuje się za pomocą tej komendy 
4. `python add_everything.py` - przetwarzanie danych oraz dodadanie dodatkowych informacji do artykułow

