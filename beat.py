import sys
import time

from scrap_bankier import run_bankier
from scrap_businessinsider import run_buisnessinsider
from scrap_money import run_money


def scrap():
    run_buisnessinsider()
    run_money()
    run_bankier()


def run(frequency_in_sec):
    while True:
        scrap()
        time.sleep(frequency_in_sec)


if __name__ == "__main__":
    frequency_in_sec = sys.argv[1]
    run(frequency_in_sec)