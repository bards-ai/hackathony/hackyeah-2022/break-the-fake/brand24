from bs4 import BeautifulSoup
from urllib.parse import urlparse
import requests
import json
from tqdm import tqdm

from scrap_bankier import get_all_articles, get_json, save_json
import datetime
  

def get_all_news_links(url: str, pages: int):
    all_links = []
    for i in range(1, pages+1):
        html = requests.get(f"{url}?strona={i}").text
        soup = BeautifulSoup(html, "html.parser")
        main = soup.find("article")

        all_news = main.find_all("a")
        links = [news.get("href", default="") for news in all_news]
        links = [link for link in links if link.startswith("/gospodarka/") and len(link) > 53]
        links = [f"https://www.money.pl{link}" for link in links]
        all_links += links
    return all_links


def get_article(url_article: str):
    html = requests.get(url_article).text
    soup = BeautifulSoup(html, "html.parser")

    title_html = soup.find("h1")
    title = ""
    if title_html:
        title = title_html.text
    

    summary_html = soup.find("p")
    summary = ""
    if summary_html:
        summary = summary_html.text.lstrip().rstrip() or " "
    summary

    
    source_name = urlparse(url_article).netloc.split(".")[1] or " "

    date_html = soup.find("time")
    date = ""
    if date_html:
        date = date_html.text.strip()
    date

    
    author_html = soup.select_one("#app > div > div > div > div > div > div > div > div > div > div > div > span > span")
    author = ""
    if author_html:
        author = author_html.text
    author

    tags = []
    tags_elements = soup.select("#app > div > div > div > div > div > div > div > div > div > div > article > div ")
    if tags_elements and len(tags_elements) >= 3:
        divs = tags_elements[-3].find_all("div")
        if divs:
            tags = list(set([div.text for div in divs if div.text != ""]))


    article_html = soup.find("article")
    text = ""
    if article_html:
        article_ps = article_html.find_all("p")
        if article_ps:
            text = " ".join([p.text for p in article_ps])
            text = " ".join(text.split())


    article = {
        "title": title,
        "summary": summary,
        "link": url_article,
        "source": source_name,
        "date": date,
        "author": author,
        "tags": tags,
        "text": text,
        "category": "",
        "sentiment": "",
        "is_fake": "",
    }

    return article

def get_all_articles(links):
    articles = []
    for link in tqdm(links):
        article = get_article(link)
        articles.append(article)
    return articles

def run_money():
    current_time = datetime.datetime.now()

    links = get_all_news_links("https://www.money.pl/gospodarka/wiadomosci/", pages=30)
    links = list(set(links))
    save_json(links, f"money-links-30-{current_time}.json")
    links = get_json(f"money-links-30-{current_time}.json")
    articles = get_all_articles(links)
    save_json(articles, f"money-articles-30-{current_time}.json")

if __name__ == "__main__":
    run_money()