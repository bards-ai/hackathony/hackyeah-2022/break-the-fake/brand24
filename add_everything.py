from add_category import categories_to_lemmas, set_categories
from add_sentiment import add_sentiment
import spacy

from add_translation import translate_summaries
from scrap_businessinsider import get_json, save_json


def add_everything(filename):
    articles = get_json(f"{filename}.json")
    articles_translated = translate_summaries(articles)
    save_json(articles_translated, f"{filename}-translated.json")

    articles_with_sentiment = add_sentiment(articles_translated)
    save_json(articles_with_sentiment, f"{filename}-sentiment.json")

    category_lemmas = categories_to_lemmas()
    articles_with_category = set_categories(articles_with_sentiment, category_lemmas)
    save_json(articles_with_category, f"{filename}-categories.json")

if __name__ == "__main__":
    add_everything("money-articles-30")


