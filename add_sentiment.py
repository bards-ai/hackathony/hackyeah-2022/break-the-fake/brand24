from transformers import pipeline
from tqdm import tqdm

from scrap_businessinsider import get_json, save_json

sentiment_analysis = pipeline("sentiment-analysis",model="siebert/sentiment-roberta-large-english")

def add_sentiment(articles):
    for article_idx, article in enumerate(tqdm(articles)):
        summary = article["translated_summary"]
        summary_sentiment = sentiment_analysis(summary)[0]
        articles[article_idx]["sentiment_label"] = summary_sentiment["label"]
        articles[article_idx]["sentiment"] = summary_sentiment["score"]

    return articles

if __name__ == "__main__":
    articles = get_json("businessinsider_articles-30-translated.json")
    articles_with_sentiment = add_sentiment(articles)
    save_json(articles_with_sentiment, "businessinsider_articles-30-sentiment.json")
