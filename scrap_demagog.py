from bs4 import BeautifulSoup
from urllib.parse import urlparse
import requests
import json
import time
from tqdm import tqdm

def get_page_soup(url):
    html = requests.get(url).text
    return BeautifulSoup(html, "html.parser")

def get_no_pages(page_soup):
    return page_soup.find_all('a', {'class': 'page-numbers'})[-2].text


def get_articles(page_soup):
    section = page_soup.find('div', {"id":"response"})
    return section.find_all('article')

def get_article_title(article_soup):
    return article_soup.find('a')['title']


def save_json(object, filename: str):
    with open(filename, "a+", encoding="utf-8") as f:
        json.dump(object, f, ensure_ascii=False, indent=4)

def get_json(filename):
    with open(filename) as f:
        object = json.load(f)
    return object


if __name__ == "__main__":

    BASE_URL='https://demagog.org.pl/fake_news'

    main_page_soup = get_page_soup(BASE_URL)
    no_pages = get_no_pages(main_page_soup)

    titles = []
    for page_num in tqdm(range(int(no_pages))):
        page_soup = get_page_soup(f'{BASE_URL}/page/{page_num}')
        for article in get_articles(page_soup):
            time.sleep(0.2)
            titles.append({"fake_news_title": get_article_title(article)})
    
    save_json(titles, 'demagog_fake_titles.json')

