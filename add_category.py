import spacy
from scrap_businessinsider import get_json, save_json
from tqdm import tqdm

categories = {
    "Ministerstwo Finansów": [
        "Ministerstwo Finansów",
        "Minister Finansów",
        "Rzecznik Prasowy MF",
        "Generalny Inspektor Informacji Finansowej (GIIF)",
        "Wiceminister finansów",
    ],
    "Finanse Publiczne": [
        "Budżet państwa",
        "Dług publiczny",
        "Stabilizująca Reguła Wydatków (SRW)",
        "Obligacje skarbowe",
        "Deficyt budżetowy",
        "Wieloletni Plan Finansowy Państwa",
        "Gwarancje i Finansowanie",
        "Gry Hazardowe",
    ],
    "Podatki i cło": ["Podatki", "cło", "PIT", "CIT", "Akcyza", "VAT"],
    "Administracja Skarbowa": [
        "Krajowa Administracja Skarbowa",
        "Szef Krajowej Administracji Skarbowej",
        "Rzecznik prasowy Szefa KAS",
        "Izba Administracji Skarbowej",
        "Urząd Skarbowy",
        "Służba Celno-Skarbowa",
        "Funkcjonariusze Celno-Skarbowi",
        "Krajowa Informacja Skarbowa",
        "Krajowa Szkoła Skarbowości",
    ],
    "Projekty Ministar Finansów": [
        "Polski Ład",
        "Twój e-Pit",
        "e-Urząd Skarbowy",
        "Slim VAT",
        "Aktualizacja Programu Konwergencji",
        "Podatek Reklamowy",
        "Estoński CIT",
        "Strategia Rozwoju Rynku Kapitałowego",
        "Głos Podatnika",
        "Finansoaktywni",
        "Polska Agencja Nadzoru Audytowego",
        "Centralny Rejestr Beneficjentów Rzeczywistych",
        "Wakacje Kredytowe",
    ],
    "Instytucje Finansowe": [
        "Naodowy Bank Polski",
        "Rada Polityki Pieniężnej",
        "Międzynarodowy Fundusz Walutowy",
        "Komisja Nadzoru Finansowego",
    ],
    "Przedstawiciele MF": [
        "Magdalena Rzeczkowska",
        "Anna Chałupa",
        "Piotr Patkowski",
        "Sebastian Skuza",
        "Łukasz Czernicki",
        "Katarzyna Szwarc",
        "Mariusz Gojny",
        "Artur Soboń",
        "Katarzyna Szweda",
        "Bartosz Zbaraszczuk",
        "Justyna Pasieczyńska",
        "Patrycja Dudek",
    ],
    "System Poboru Opłat": [
        "Viatoll",
        "e-Toll",
        "e-myto",
    ],
    "Krajowy Plan Odbudowy": ["KPO"],
}

nlp = spacy.load("pl_core_news_lg")

def categories_to_lemmas():
    categories_lemmas = {}
    for category, topics in categories.items():
        if category not in topics:
            topics.append(category)
        new_topics = []
        for topic in topics:
            doc = nlp(topic)
            topic_lemma = ""
            for token in doc:
                topic_lemma += f"{token.lemma_} "
            new_topics.append(topic_lemma.rstrip().replace(" - ", "-").lower())
        categories_lemmas[category] = new_topics
    return categories_lemmas

def set_categories(articles, category_lemmas):
    for article in tqdm(articles):
        text = article["text"]
        doc = nlp(text)
        text_in_lemmas = " ".join([token.lemma_.lower() for token in doc])
        for category, topics in category_lemmas.items():
            for topic in topics:
                if topic in text_in_lemmas:
                    article["category"] = category

    return articles




if __name__ == "__main__":
    articles = get_json("businessinsider_articles-30-sentiment.json")
    category_lemmas = categories_to_lemmas()
    articles_with_category = set_categories(articles, category_lemmas)
    save_json(articles_with_category, "businessinsider_articles-30-categories.json")

